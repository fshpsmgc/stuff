﻿/*
	Заданы два натуральных числа a, b 
	которые обозначают число и месяц 
	не високосного года. Разработать алгоритм 
	и программу для вычисления порядкового номера 
	даты, начиная отсчет с начала года. Например, 
	число 2.03 (2 марта) имеет порядковый номер 61. 
	Год считать не високосным.
*/

#include <iostream>

int getDate(int day, int month);
int getDays(int x);

int main()
{
	setlocale(LC_ALL, "Russian");

	int day, month;
	int maxDay;

	//Ввод месяца
	do {
		std::cout << "Введите месяц: ";
		std::cin >> month;
		if (month <= 0 || month > 12) std::cout << "Некорректный ввод\n";
	} while (month <= 0 || month > 12);

	maxDay = getDays(month);

	//Ввод дня
	do {
		std::cout << "Введите день: ";
		std::cin >> day;
		if (day <= 0 || day > maxDay) std::cout << "Некорректный ввод\n";
	} while (day <= 0 || day > maxDay);

	//Получение порядкового номера
	std::cout << getDate(day, month) << "\n";
}

//Получение порядкового номера
int getDate(int day, int month) {
	int days = 0;

	for (int i = 1; i < month; i++) {
		days += getDays(i);
	}

	return days + day;
}

//https://habr.com/ru/post/261773/
int getDays(int x) {
	return 28 + (x + (x / 8)) % 2 + 2 % x + 2 * (1 / x);
}