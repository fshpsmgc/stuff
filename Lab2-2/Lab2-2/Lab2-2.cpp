﻿/*Разработать программу для вычисления интеграла с заданной точностью методом прямоугольников и методом трапеций,
  оформив каждый способ в виде отдельной функции. Вывести на экран результаты интегрирования разными методами для сравнения.
  Подынтегральное выражение оформить в виде отдельной функции.
*/

#include <iostream>

const double pi = 3.141592653589793238462643383279502884L;

double rect(double, double, double);
double f(double);
double trap(double, double, double);

using namespace std;
int main()
{
	setlocale(LC_ALL, "Russian");

	//Ввод точности (от 0 до 1)
	double precision = 0;
	cout << "Точность: ";
	cin >> precision;

	//Вычисление интеграла
	cout << "Метод трапеций: " << trap(0, pi/2, precision) << "\n";
	cout << "Метод прямоугольников: " << rect(0, pi/2, precision) << "\n";

}

//Метод трапеций
double trap(double a, double b, double eps) {
	if (eps <= 0) return 0;		//Точность не может быть меньше нуля
	//Инициализация
	double y = 0;
	double dy = 0;
	double c = 1;
	double p = 0;
	int n = 100;

	while (abs(p - c) > eps) { //Проверка точности
		p = c;

		//Вычисление интеграла
		dy = (b - a) / n;
		y = f(a) + f(b);
		for (int i = 0; i < n; i++) {
			y += 2 * (f(a + dy * i));
		}
		c = ((b - a) / (2 * n) * y);

		n *= 2;
	}

	return c;
}

//Метод прямоугольников
double rect(double a, double b, double eps) {
	if (eps <= 0) return 0;		//Точность не может быть меньше нуля
	//Инициализация
	double y = 0;
	double dy = 0;
	double p = 0;
	double c = 1;
	int n = 100;
	
	while (abs(p - c) > eps) {	//Проверка точности
		p = c;

		//Вычисление интеграла
		dy = (b - a) / n;
		y = (f(a) + f(b)) / 2;
		for (int i = 0; i < n; i++) {
			y += f(a + i * dy);
		}
		c = y * dy;

		n *= 2;
	}

	return c;
}

//Подынтегральная функция
double f(double x) {
	return sin(x) / sqrt((x * x) + 1);
}