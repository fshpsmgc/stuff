﻿/*Составить программу обработки динамической структуры данных:
по стеку L построить два стека: L1 – из элементов, имеющих
четное значение веса, и L2 –  из элементов, имеющих нечетное
значение веса.
*/

#include <iostream>

//Элемент стека
struct Stack {
	int i;
	Stack* next;
};

Stack *L, *L1, *L2;

int pop(Stack*&);
void push(Stack*&, int);
int peek(Stack*);
void sort(Stack*&, Stack*&, Stack*&);
void show(Stack*&);
void clear(Stack*&);

using namespace std;
int main()
{
	setlocale(LC_ALL, "Russian");

	int menu;
	int temp = 0;
	do {
		cout << "1.Добавить\n2.Достать\n3.Посмотреть на верхний элемент\n4.Сортировать\n5.Вывод L1 и L2\n0.Выход\nВвод: ";
		cin >> menu;

		switch (menu) {
		case 1:
			int i;
			cout << "Значение: ";
			cin >> i;
			push(L, i);					//Добавить значение в стек
			cout << "\n";
			break;
		case 2:
			temp = pop(L);
			if (temp != NULL) {
				cout << "Убранное значение: " << temp << "\n";			//Достать значение из стека
			}
			cout << "\n";
			break;
		case 3:
			temp = peek(L);
			if (temp != NULL) {

				cout << "Верхний элемент: " << peek(L) << "\n";		//Посмотреть на последнее значение в стеке
			}
			cout << "\n";
			break;
		case 4:
			sort(L, L1, L2);
			break;
		case 5:
			cout << "L1: ";
			show(L1);
			cout << "\nL2: ";
			show(L2);
			cout << "\n";
			break;
		}
	} while (menu != 0);
}

//Удаляет верхний элемент стека и возвращает его значение
int pop(Stack *&head) {
	if (head == nullptr) {				//Проверка стека на пустоту
		cout << "Пустой стек\n";
		return NULL;
	}

	int i = head->i;
	head = head->next;
	return i;
}

//Добавляет элемент со значением i в стек
void push(Stack *&head, int i) {
	Stack* temp = new Stack();
	temp->i = i;
	temp->next = head;
	head = temp;
}

//Просмотр верхнего элемента стека
int peek(Stack* head) {
	if (head == NULL) {			//Проверка стека на пустоту
		cout << "Пустой стек\n";
		return NULL;
	}

	return head->i;
}

void sort(Stack *&src, Stack *&even, Stack *&odd) {
	int v;
	clear(even);
	clear(odd);

	while (src) {
		v = pop(src);	//Достаём каждый элемент
		if (v % 2 == 0) {
			push(even, v); //Если чётный, добавляем в L1
		}
		else {
			push(odd, v); //Иначе в L2
		}
	}

	cout << "Сортировка завершена\n\n";
}

void show(Stack *& head) {
	while (head) {
		cout << pop(head) << " ";
	}

	cout << "\n";
}

void clear(Stack*& head) {
	while (head) {
		pop(head);
	}
}